package com.luv2code.junitdemo;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.api.condition.EnabledIfSystemProperties;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;

import java.time.Duration;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.IndicativeSentences.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DemoUtilsTest {
    DemoUtils demoUtils;

    @BeforeEach
    void initilizeDemoUtils() {
        System.out.println("Before test");
        demoUtils = new DemoUtils();
    }

    @AfterEach
    void afterTests() {
        System.out.println("After test");
    }

    @BeforeAll
    static void beforeAllTests() {
        System.out.println("Print this before all tests");
    }

    @Test
    @DisplayName("Srkoman")
    void givenDemoUtils_whenCheckAdd_willReturnInteger() {
        // setup/given is done in initilizeDemoUtils method

        // execute/when
        Integer resultOfAddMethod = demoUtils.add(3, 2);

        // assert/than
        assertEquals(5, resultOfAddMethod, "expect 5");
        assertNotEquals(6, demoUtils.add(4, 5), "expect that this is not true");
    }

    @Test
    @Order(1)
    void givenDemoUtils_whenCheckNull_willReturnNull() {
        // setup/given
        String nullValue = null;
        String notNullValue = "some value";

        // execute/when
        Object checkNullResult = demoUtils.checkNull(nullValue);

        // assert/than
        assertNull(checkNullResult, "check if value is null");
        assertNotNull(demoUtils.checkNull(notNullValue));
    }

    @Test
    void testSameOrNotSame() {
        assertSame(demoUtils.getAcademy(), demoUtils.getAcademyDuplicate(), "this must be same");
        assertNotSame(demoUtils.getAcademy(), "srkoman", "This should not be the same");
    }

    @Test
    void testTrueOrFalse() {
        assertTrue(demoUtils.isGreater(2, 1), "This should return true");
        assertFalse(demoUtils.isGreater(1, 2), "This should be return false");
    }

    @Test
    void testAreArraysEquals() {
        List<String> pomIterable = List.of("luv", "2", "code");
        assertArrayEquals(new String[]{"A", "B", "C"}, demoUtils.getFirstThreeLettersOfAlphabet(), "Shold be ==");
        assertIterableEquals(pomIterable, demoUtils.getAcademyInList(), "Iterables should be the samoe");
    }

    @Test
    void testThrowOrNotThrow() {
        assertThrows(Exception.class, () -> demoUtils.throwException(-1), "should throw exception");
        assertDoesNotThrow(()->demoUtils.throwException(1));
    }

    @Test
    void testTimeout(){
        assertTimeoutPreemptively(Duration.ofSeconds(3), ()->demoUtils.checkTimeout(), "Method should execute in 3s");
    }

    @Test
    void testMultiple() {
        System.out.println("srk");
        System.out.println(demoUtils.multiply(1,2));
//        assertEquals(6, demoUtils.multiply(2,3));
    }

    @Test
    @EnabledIfEnvironmentVariable(named = "srk1", matches = "srkoman1")
    void srkoman() {
        System.out.println("Test env variable");
    }

    @Test
    @Disabled("This test is disabled")
    @EnabledIfSystemProperty(named = "srk2", matches = "srkoman2")
    void srkoman2() {
        System.out.println("Test sys variable");
    }


}
